<?php

class solution
{
    /**
     * Znajduje liczby, które się nie powtarzają
     *
     * @param $input array Tablica liczb
     * @return array
     */
    public static function findSingle(array $input): array
    {
        $result = [];
        foreach ($input as $value) {
            if (count(array_keys($input, $value)) == 1) {
                $result[] = $value;
            }
        }
        return $result;
    }
}

//print_r(solution::findSingle([1, 2, 3, 4, 1, 2, 3]));
// Array
// (
//     [0] => 4
// )


//print_r(solution::findSingle([11, 21, 33.4, 18, 21, 33.39999, 33.4]));
// Array
// (
//     [0] => 11
//     [1] => 33.39999
//     [2] => 18
// )
