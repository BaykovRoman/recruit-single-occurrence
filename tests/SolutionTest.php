<?php

namespace Tests;


use PHPUnit\Framework\TestCase;
use solution;

class SolutionTest extends TestCase
{
    public function testFindSingle()
    {
        $this->assertEquals(solution::findSingle([1, 2, 3, 4, 1, 2, 3]), [4]);
        $this->assertEquals(solution::findSingle([11, 21, 33.4, 18, 21, 33.39999, 33.4]), [11, 18, 33.39999]);
        $this->assertEquals(solution::findSingle([1, 1]), []);
    }
}
